# Docker With NodeJS

This examples shows how to use Docker, exemplifying by using a NodeJS Web Application that uses local storage to read and write in a file.

### Environment
  - Docker 1.13.x

## Build The Image
    docker build . -t node-test:1.0.0

## Run The Container
    docker run --name nodejs -it --rm -p 8080:8080 node-test:1.0.0

## Run The Container
    docker volume create nodejs-test
    docker run --name nodejs -it --rm -p 8080:8080 -v  nodejs-test:/tmp/nodejs-file-handler node-test:1.0.0
