var http = require('http');
var fs = require('fs');

const filePath = '/tmp/nodejs-file-handler/file.txt';

http.createServer( function (req, res) {

if(req.method == "POST"){
  let body = [];
  req.on('data', (chunk) => {
    body.push(chunk);
  }).on('end', () => {
        body = Buffer.concat(body).toString();
        // at this point, `body` has the entire request body stored in it as a string
        console.log("Message received : " + body);
        fs.appendFile(filePath, body + "\n", (err) => {
            if (err) throw err;
            res.writeHead(201, {'Content-Type': 'text/plain'});
            res.end(`Message Saved`);
        });
  });

}

if(req.method == "GET"){

  fs.readFile(filePath, 'utf8', function(err, contents) {
   res.writeHead(200, {'Content-Type': 'text/plain'});
   res.end(contents);
  });

}

}).listen(8080, '0.0.0.0');

console.log('Server running ');
