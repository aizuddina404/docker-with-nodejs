FROM node:6.17.0-alpine

EXPOSE 8080

WORKDIR /app

COPY  *  /app/

RUN mkdir /tmp/nodejs-file-handler/

VOLUME  /tmp/nodejs-file-handler/

CMD ["npm" ,"start"]
